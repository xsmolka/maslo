import spot
import numpy as np

from subfiles.sims import lnlhw_lnhw_fsim, lnlhw_lnhw_bsim
from subfiles.maximal_transitive_subrelation import mtssym
from subfiles.utilities import is_may_edge

spot.setup()


def quotienting(automaton, args):
    if args.direction == "fw":
        sim = lnlhw_lnhw_fsim(automaton, args.n)
    elif args.direction == "bw":
        sim = lnlhw_lnhw_bsim(automaton, args.n)
    equiv = get_sym(sim)
    mtssym(equiv)
    return must_quotient(automaton, equiv)


def must_quotient(aut, equiv):
    dic = spot.bdd_dict()
    quotient = spot.make_twa_graph(dic)
    acc = spot.acc_code('Inf(0)')
    quotient.set_acceptance(1, acc)
    old_to_new = dict()
    hell = aut.num_states()
    for i in range(aut.num_states()):
        class_cr, class_n = class_created(i, equiv, old_to_new)
        if class_cr:
            old_to_new[i] = class_n
        else:
            new_n = quotient.new_state()
            old_to_new[i] = new_n
    old_init_state_n = aut.get_init_state_number()
    quotient.set_init_state(old_to_new[old_init_state_n])
    quotient.copy_ap_of(aut)
    for e in aut.edges():
        if e.src == hell or e.dst == hell:
            continue
        en = aut.edge_number(e)
        if is_may_edge(aut, en):
            continue
        new_src = old_to_new[e.src]
        new_dst = old_to_new[e.dst]
        quotient.new_edge(new_src, new_dst, e.cond, e.acc)
    return quotient


def class_created(i, rel, old_to_new):
    for j in range(len(rel)):
        if i == j:
            continue
        if rel[i, j] == 1 and j in old_to_new.keys():
            return True, old_to_new[j]
    return False, None


def get_sym(rel):
    n = len(rel)
    srel = np.zeros([n, n], dtype=bool)
    for x in range(n):
        for y in range(n):
            if rel[x, y] and rel[y, x]:
                srel[x, y] = True
                srel[y, x] = True
    return srel
