import buddy


def implies(a, b):
    return not a or b


def is_may_edge(automaton, edge_num):
    return automaton.get_highlight_edge(edge_num)


def make_must_edge(automaton, en):
    automaton.highlight_edge(en, None)


def make_may_edge(automaton, en):
    automaton.highlight_edge(en, 1)


def remove_edge(automaton, en):
    for state in range(automaton.num_states()):
        oi = automaton.out_iteraser(state)
        while oi:
            n = automaton.edge_number(oi.current())
            if en == n:
                oi.erase()
            else:
                oi.advance()


def pg_add_eden(pgdata):
    eden = pgdata.arena.new_state()
    pgdata.owners.append(True)
    for i in range(pgdata.arena.num_states()):
        if i != eden:
            pgdata.arena.new_edge(eden, i, buddy.bddtrue, [0])
    return eden


def get_low_high(automaton):
    high = set(automaton.edge_number(e) for e in automaton.edges())
    low = set(filter(lambda en: not is_may_edge(automaton, en), high))
    return low, high