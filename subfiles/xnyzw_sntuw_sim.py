import spot
import buddy
from bidict import bidict
import numpy as np

from subfiles.utilities import pg_add_eden, implies

spot.setup()


class PGData:
    def __init__(self, automaton, arena, maps, owners, odd_states,
                 spoiler_n, spoiler_n1, spoiler_w,
                 duplicator_n, duplicator_n1, duplicator_w,
                 n, match_initial):
        self.automaton = automaton
        self.arena = arena
        self.maps = maps
        self.owners = owners
        self.odd_states = odd_states
        self.spoiler_n = spoiler_n
        self.spoiler_n1 = spoiler_n1
        self.spoiler_w = spoiler_w
        self.duplicator_n = duplicator_n
        self.duplicator_n1 = duplicator_n1
        self.duplicator_w = duplicator_w
        self.match_initial = match_initial
        self.n = n


def xnyzw_sntuw_sim(automaton, aut_hell, spoiler_n, spoiler_n1, spoiler_w,
                    duplicator_n, duplicator_n1, duplicator_w,
                    n, match_initial=False):
    arena, maps, pg_eden = create_parity_arena(automaton, spoiler_n,
                                               spoiler_n1, spoiler_w,
                                               duplicator_n, duplicator_n1,
                                               duplicator_w, n, match_initial)
    sim_rel = np.identity(automaton.num_states(), dtype=bool)
    spot.solve_parity_game(arena)
    for n in range(arena.num_states()):
        if n == pg_eden:
            continue
        if len(maps.inverse[n]) > 3:
            continue
        (p, q, i) = maps.inverse[n]
        if p == aut_hell or q == aut_hell or i != 0 or p == q:
            continue
        if arena.get_state_winner(n):
            sim_rel[p, q] = True
    return sim_rel


def create_parity_arena(automaton, spoiler_n, spoiler_n1, spoiler_w,
                        duplicator_n, duplicator_n1, duplicator_w,
                        n, match_initial):
    maps = bidict(dict())
    owners = []
    dic = spot.bdd_dict()
    arena = spot.make_twa_graph(dic)
    acc = spot.acc_code("parity max odd 1")
    arena.set_acceptance(1, acc)
    odd_states = set()
    pgdata = PGData(automaton, arena, maps, owners, odd_states,
                    spoiler_n, spoiler_n1, spoiler_w,
                    duplicator_n, duplicator_n1, duplicator_w,
                    n, match_initial)
    create_even_states(pgdata)
    create_odd_to_even_edges(pgdata)
    eden = pg_add_eden(pgdata)
    arena.set_init_state(eden)
    spot.set_state_players(arena, owners)
    return arena, maps, eden


def create_even_states(pgdata):
    num_states = pgdata.automaton.num_states()
    init_state_n = pgdata.automaton.get_init_state_number()
    for p in range(num_states):
        for q in range(num_states):
            if ((not pgdata.match_initial) or
                    implies(p == init_state_n, q == init_state_n)):
                for i in range(pgdata.n + 2):
                    n_state = pgdata.arena.new_state()
                    pgdata.owners.append(False)
                    pgdata.maps[(p, q, i)] = n_state
                    create_even_to_odd_edges(n_state, p, q, i, pgdata)


def create_even_to_odd_edges(e_state, p, q, i, pgdata):
    if i < pgdata.n:
        trans = pgdata.spoiler_n
    elif i == pgdata.n:
        trans = pgdata.spoiler_n1
    else:
        trans = pgdata.spoiler_w
    for edge in pgdata.automaton.out(p):
        edge_num = pgdata.automaton.edge_number(edge)
        if edge_num not in trans:
            continue
        if ((edge.dst, q, i, edge.cond, edge.acc.has(0)) not in
                pgdata.maps.keys()):
            o_state = create_odd_state(edge.dst, q, i, edge.cond, edge.acc,
                                       pgdata)
        else:
            o_state = pgdata.maps[(edge.dst, q, i, edge.cond, edge.acc.has(0))]
        pgdata.arena.new_edge(e_state, o_state, buddy.bddtrue, [0])


def create_odd_state(p, q, i, cond, acc, pgdata):
    n_state = pgdata.arena.new_state()
    pgdata.odd_states.add(n_state)
    pgdata.maps[(p, q, i, cond, acc.has(0))] = n_state
    pgdata.owners.append(True)
    return n_state


def create_odd_to_even_edges(pgdata):
    for s in pgdata.odd_states:
        (p, q, i, cond, bacc) = pgdata.maps.inverse[s]
        if i < pgdata.n:
            trans = pgdata.duplicator_n
        elif i == pgdata.n:
            trans = pgdata.duplicator_n1
        else:
            trans = pgdata.duplicator_w
        acc = spot.mark_t([0]) if bacc else spot.mark_t([])
        for edge in pgdata.automaton.out(q):
            edge_num = pgdata.automaton.edge_number(edge)
            if edge_num not in trans:
                continue
            if (buddy.bdd_implies(cond, edge.cond) and
                    acc.subset(edge.acc) and
                    ((p, edge.dst, min(i + 1, pgdata.n + 1)) in
                     pgdata.maps.keys())):
                layer = min(i + 1, pgdata.n + 1)
                pgdata.arena.new_edge(s, pgdata.maps[(p, edge.dst, layer)],
                                      buddy.bddtrue, [1])