import numpy as np


# Algorithm for computing maximal transitive subrelation described
# in the paper by Chakraborty et al.
#
# CHAKRABORTY, Sourav; GHOSH, Shamik; JHA, Nitesh; ROY,
# Sasanka. Maximal and Maximum Transitive Relation Contained
# in a Given Binary Relation. In: XU, Dachuan; DU, Donglei; DU,
# Ding-Zhu (eds.). Computing and Combinatorics - 21st International
# Conference, COCOON 2015, Beijing, China, August 4-6, 2015, Pro-
# ceedings. Springer, 2015, vol. 9198, pp. 587–600. Lecture Notes in
# Computer Science. Available from DOI: 10.1007/978-3-319-
# 21398-9\_46.

def mts(rel: np.matrix) -> None:
    n = len(rel)
    for i in range(0, n):
        bi = []
        for j in range(0, n):
            if j == i:
                continue
            if rel[i, j]:
                bi.append(j)
        for j in bi:
            for k in range(0, n):
                if k != j and not rel[i, k]:
                    rel[j, k] = False
                if k != i and not rel[k, j]:
                    rel[k, i] = False
    return


# Modified version of the previous algorithm, for computing maximal
# symmetric transitive subrelation.

def mtssym(rel: np.matrix) -> None:
    n = len(rel)
    for i in range(0, n):
        bi = []
        for j in range(0, n):
            if j == i:
                continue
            if rel[i, j]:
                bi.append(j)
        for j in bi:
            for k in range(0, n):
                if k != j and not rel[i, k]:
                    rel[j, k] = False
                    rel[k, j] = False
                if k != i and not rel[k, j]:
                    rel[k, i] = False
                    rel[i, k] = False
    return
