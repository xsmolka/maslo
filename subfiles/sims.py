import spot
import buddy

from subfiles.xnyzw_sntuw_sim import xnyzw_sntuw_sim
from subfiles.lw_hw_sim import lw_hw_sim
from subfiles.utilities import make_may_edge, is_may_edge, get_low_high

spot.setup()


def lw_hw_fsim(automaton):
    hell = add_hell(automaton)
    low, _ = get_low_high(automaton)
    sim, arena, s_maps, e_maps = lw_hw_sim(automaton, hell, low)
    automaton.kill_state(hell)
    # Sometimes the edges have different numbers after purging dead states
    # so we cannot purge them to preserve information from simulation.
    # automaton.purge_dead_states()
    return sim, arena, s_maps, e_maps


def lw_hw_bsim(automaton):
    inverted = inverted_automaton(automaton)
    hell = add_hell(inverted)
    low, _ = get_low_high(inverted)
    sim, arena, s_maps, e_maps = lw_hw_sim(inverted, hell, low, True)
    inverted.kill_state(hell)
    # inverted.purge_dead_states()
    return sim, arena, s_maps, e_maps


def hnlw_hnhlw_fsim(automaton, n):
    hell = add_hell(automaton)
    low, high = get_low_high(automaton)
    sim = xnyzw_sntuw_sim(automaton, hell, high, low, low, high, high, low, n)
    automaton.kill_state(hell)
    automaton.purge_dead_states()
    return sim


def hnlw_hnhlw_bsim(automaton, n):
    inverted = inverted_automaton(automaton)
    hell = add_hell(inverted)
    low, high = get_low_high(inverted)
    sim = xnyzw_sntuw_sim(inverted, hell, high, low, low, high, high, low, n,
                          True)
    inverted.kill_state(hell)
    inverted.purge_dead_states()
    return sim


def lnlhw_lnhw_fsim(automaton, n):
    hell = add_hell(automaton)
    low, high = get_low_high(automaton)
    sim = xnyzw_sntuw_sim(automaton, hell, low, low, high, low, high, high, n)
    automaton.kill_state(hell)
    automaton.purge_dead_states()
    return sim


def lnlhw_lnhw_bsim(automaton, n):
    inverted = inverted_automaton(automaton)
    hell = add_hell(inverted)
    low, high = get_low_high(inverted)
    sim = xnyzw_sntuw_sim(inverted, hell, low, low, high, low, high, high, n,
                          True)
    inverted.kill_state(hell)
    inverted.purge_dead_states()
    return sim


def inverted_automaton(aut):
    dic = spot.bdd_dict()
    inv = spot.make_twa_graph(dic)
    inv.new_states(aut.num_states())
    for e in aut.edges():
        en = aut.edge_number(e)
        new_en = inv.new_edge(e.dst, e.src, e.cond, e.acc)
        if is_may_edge(aut, en):
            make_may_edge(inv, en)
    inv.set_init_state(aut.get_init_state_number())
    return inv


def add_hell(automaton):
    hell = automaton.new_state()
    num_states = automaton.num_states()
    for i in range(num_states):
        automaton.new_edge(i, hell, buddy.bddtrue)
    return hell
