import buddy
from bidict import bidict
import numpy as np
import spot
from subfiles.utilities import implies, pg_add_eden

spot.setup()


class PGData:
    def __init__(self, automaton, arena, s_maps, e_maps, owners, odd_states,
                 low, match_initial):
        self.automaton = automaton
        self.arena = arena
        self.s_maps = s_maps
        self.e_maps = e_maps
        self.owners = owners
        self.odd_states = odd_states
        self.low = low
        self.match_initial = match_initial


def lw_hw_sim(automaton, aut_hell, low, match_initial=False):
    arena, s_maps, e_maps, pg_eden = create_parity_arena(automaton, low,
                                                         match_initial)
    sim_rel = np.identity(automaton.num_states(), dtype=bool)
    spot.solve_parity_game(arena)
    for n in range(arena.num_states()):
        if n == pg_eden:
            continue
        if len(s_maps.inverse[n]) > 3:
            continue
        (p, q) = s_maps.inverse[n]
        if p == aut_hell or q == aut_hell or p == q:
            continue
        if arena.get_state_winner(n):
            sim_rel[p, q] = True
    return sim_rel, arena, s_maps, e_maps


def create_parity_arena(automaton, low, match_initial):
    s_maps = bidict(dict())
    e_maps = dict()
    owners = []
    dic = spot.bdd_dict()
    arena = spot.make_twa_graph(dic)
    acc = spot.acc_code("parity max odd 1")
    arena.set_acceptance(1, acc)
    odd_states = set()
    pgdata = PGData(automaton, arena, s_maps, e_maps, owners, odd_states, low,
                    match_initial)
    create_even_states(pgdata)
    create_odd_to_even_edges(pgdata)
    eden = pg_add_eden(pgdata)
    arena.set_init_state(eden)
    spot.set_state_players(arena, owners)
    return arena, s_maps, e_maps, eden


def create_even_states(pgdata):
    num_states = pgdata.automaton.num_states()
    init_state_n = pgdata.automaton.get_init_state_number()
    for p in range(num_states):
        for q in range(num_states):
            if ((not pgdata.match_initial) or
                    implies(p == init_state_n, q == init_state_n)):
                n_state = pgdata.arena.new_state()
                pgdata.owners.append(False)
                pgdata.s_maps[(p, q)] = n_state
                create_even_to_odd_edges(n_state, p, q, pgdata)


def create_even_to_odd_edges(e_state, p, q, pgdata):
    for edge in pgdata.automaton.out(p):
        edge_num = pgdata.automaton.edge_number(edge)
        if edge_num not in pgdata.low:
            continue
        if ((edge.dst, q, edge.cond, edge.acc.has(0)) not in
                pgdata.s_maps.keys()):
            o_state = create_odd_state(edge.dst, q, edge.cond, edge.acc,
                                       pgdata)
        else:
            o_state = pgdata.s_maps[(edge.dst, q, edge.cond, edge.acc.has(0))]
        arena_en = pgdata.arena.new_edge(e_state, o_state, buddy.bddtrue, [0])
        pgdata.e_maps[arena_en] = edge_num


def create_odd_state(p, q, cond, acc, pgdata):
    n_state = pgdata.arena.new_state()
    pgdata.odd_states.add(n_state)
    pgdata.s_maps[(p, q, cond, acc.has(0))] = n_state
    pgdata.owners.append(True)
    return n_state


def create_odd_to_even_edges(pgdata):
    for s in pgdata.odd_states:
        (p, q, cond, bacc) = pgdata.s_maps.inverse[s]
        acc = spot.mark_t([0]) if bacc else spot.mark_t([])
        for edge in pgdata.automaton.out(q):
            if (buddy.bdd_implies(cond, edge.cond) and acc.subset(edge.acc) and
                    (p, edge.dst) in pgdata.s_maps.keys()):
                arena_en = pgdata.arena.new_edge(s,
                                                 pgdata.s_maps[(p, edge.dst)],
                                                 buddy.bddtrue, [1])
                pgdata.e_maps[arena_en] = pgdata.automaton.edge_number(edge)
