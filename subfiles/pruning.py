import spot
import numpy as np
import buddy

from subfiles.sims import hnlw_hnhlw_fsim, hnlw_hnhlw_bsim
from subfiles.maximal_transitive_subrelation import mts

spot.setup()


def pruning(automaton, args):
    if args.direction == "fw":
        fsim = hnlw_hnhlw_fsim(automaton, args.n)
        bsim = np.identity(automaton.num_states(), dtype=bool)
    elif args.direction == "bw":
        bsim = hnlw_hnhlw_bsim(automaton, args.n)
        fsim = np.identity(automaton.num_states(), dtype=bool)
    edge_rel = get_assym(create_edge_rel(automaton, fsim, bsim))
    mts(edge_rel)
    pruned = pruned_automaton(automaton, edge_rel)
    pruned.purge_unreachable_states()
    return pruned


def pruned_automaton(aut, edges_rel):
    dic = spot.bdd_dict()
    pruned = spot.make_twa_graph(dic)
    acc = spot.acc_code('Inf(0)')
    pruned.set_acceptance(1, acc)
    pruned.new_states(aut.num_states())
    pruned.set_init_state(aut.get_init_state_number())
    pruned.copy_ap_of(aut)
    for e in aut.edges():
        en = aut.edge_number(e)
        if is_maximal(en - 1, edges_rel):
            pruned.new_edge(e.src, e.dst, e.cond, e.acc)
    return pruned


def create_edge_rel(aut, fsim, bsim):
    n = aut.num_edges()
    rel = np.zeros([n, n], dtype=bool)
    for se in aut.edges():
        for de in aut.edges():
            if bsim[se.src, de.src] and fsim[se.dst, de.dst]:
                if (buddy.bdd_implies(se.cond, de.cond) and
                        se.acc.subset(de.acc)):
                    sn = aut.edge_number(se) - 1
                    dn = aut.edge_number(de) - 1
                    rel[sn, dn] = True
    return rel


def get_assym(rel):
    n = len(rel)
    arel = np.zeros([n, n], dtype=bool)
    for x in range(n):
        for y in range(n):
            if x == y:
                continue
            if rel[x, y] and not arel[y, x]:
                arel[x, y] = True
    return arel


def is_maximal(elem, rel):
    return True not in rel[elem]
