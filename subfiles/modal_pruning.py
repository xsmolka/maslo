import numpy as np
import buddy
import spot

from subfiles.sims import lw_hw_fsim, lw_hw_bsim
from subfiles.utilities import is_may_edge, make_must_edge, remove_edge

spot.setup()


def modal_pruning(automaton, args):
    if args.direction == "fw":
        return forward_modal_pruning(automaton)
    elif args.direction == "bw":
        return backward_modal_pruning(automaton)


def forward_modal_pruning(automaton):
    check = True
    while check:
        check, automaton = modal_pruning_one_step(automaton, True)
    return automaton


def backward_modal_pruning(automaton):
    check = True
    while check:
        check, automaton = modal_pruning_one_step(automaton, False)
        return automaton


def modal_pruning_one_step(automaton, fw=True):
    if fw:
        fsim, arena, s_maps, e_maps = lw_hw_fsim(automaton)
        bsim = np.identity(automaton.num_states(), dtype=bool)
    else:
        fsim = np.identity(automaton.num_states(), dtype=bool)
        bsim, arena, s_maps, e_maps = lw_hw_bsim(automaton)
    edge_rel = create_edge_rel(automaton, fsim, bsim)
    sub_e, dom_e = choose_edge_pair(automaton, edge_rel)
    if sub_e == -1 and dom_e == -1:
        return False, automaton
    if fw:
        update_dominating_forward_traces(automaton, arena, s_maps, e_maps,
                                         sub_e, dom_e)
    else:
        update_dominating_backward_traces(automaton, arena, s_maps, e_maps,
                                          sub_e, dom_e)
    remove_edge(automaton, sub_e)
    automaton.purge_dead_states()
    return True, automaton


def update_dominating_forward_traces(automaton, arena, s_maps, e_maps,
                                     sub_e, dom_e):
    strategies = arena.get_strategy()
    arena_edges = create_edges_dict(arena)
    aut_edges = create_edges_dict(automaton)
    visited = []
    sub_edge = aut_edges[sub_e]
    dom_edge = aut_edges[dom_e]
    to_visit = [s_maps[(sub_edge.dst, dom_edge.dst)]]
    if is_may_edge(automaton, dom_e):
        make_must_edge(automaton, dom_e)
    while len(to_visit) != 0:
        current_n = to_visit.pop()
        visited.append(current_n)
        for spo_edge_arena in arena.out(current_n):
            spo_dst_state_num_arena = spo_edge_arena.dst
            dup_edge_num_arena = strategies[spo_dst_state_num_arena]
            dup_edge_arena = arena_edges[dup_edge_num_arena]
            dup_edge_num_automaton = e_maps[dup_edge_num_arena]
            if dup_edge_num_automaton in aut_edges.keys():
                if is_may_edge(automaton, dup_edge_num_automaton):
                    make_must_edge(automaton, dup_edge_num_automaton)
            new_parity_state_to_visit = dup_edge_arena.dst
            if new_parity_state_to_visit not in visited:
                to_visit.append(new_parity_state_to_visit)


def update_dominating_backward_traces(automaton, arena, s_maps, e_maps,
                                      sub_e, dom_e):
    strategies = arena.get_strategy()
    arena_edges = create_edges_dict(arena)
    aut_edges = create_edges_dict(automaton)
    visited = []
    sub_edge = aut_edges[sub_e]
    dom_edge = aut_edges[dom_e]
    to_visit = [s_maps[(sub_edge.src, dom_edge.src)]]
    if is_may_edge(automaton, dom_e):
        make_must_edge(automaton, dom_e)
    while len(to_visit) != 0:
        current_n = to_visit.pop()
        visited.append(current_n)
        for spo_edge_arena in arena.out(current_n):
            spo_dst_state_num_arena = spo_edge_arena.dst
            dup_edge_num_arena = strategies[spo_dst_state_num_arena]
            dup_edge_arena = arena_edges[dup_edge_num_arena]
            dup_edge_num_automaton = e_maps[dup_edge_num_arena]
            if dup_edge_num_automaton in aut_edges.keys():
                if is_may_edge(automaton, dup_edge_num_automaton):
                    make_must_edge(automaton, dup_edge_num_automaton)
            new_parity_state_to_visit = dup_edge_arena.dst
            if new_parity_state_to_visit not in visited:
                to_visit.append(new_parity_state_to_visit)


def choose_edge_pair(automaton, edge_rel):
    for s in range(automaton.num_edges()):
        for d in range(automaton.num_edges()):
            if s != d and edge_rel[s][d]:
                return s + 1, d + 1
    return -1, -1


def create_edge_rel(aut, fsim, bsim):
    n = aut.num_edges()
    rel = np.zeros([n, n], dtype=bool)
    for se in aut.edges():
        for de in aut.edges():
            if bsim[se.src, de.src] and fsim[se.dst, de.dst]:
                if (buddy.bdd_implies(se.cond, de.cond) and
                        se.acc.subset(de.acc)):
                    sn = aut.edge_number(se) - 1
                    dn = aut.edge_number(de) - 1
                    rel[sn, dn] = True
    return rel


def create_edges_dict(automaton):
    dictionary = dict()
    for edge in automaton.edges():
        edge_number = automaton.edge_number(edge)
        dictionary[edge_number] = edge
    return dictionary
