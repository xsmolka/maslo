import sys
import argparse
import spot

from subfiles.pruning import pruning
from subfiles.quotienting import quotienting
from subfiles.modal_pruning import modal_pruning

spot.setup()


def main():
    parser = argparse.ArgumentParser(description="""Reduce MTBA by pruning or
                                                    quotienting, utilizing
                                                    XY-simulations.""")
    parser.add_argument("technique", choices=["prune",
                                              "quotient",
                                              "modal_prune"],
                        help="""reduction technique to be used. For pruning,
                                HnLw/HnHLw-simulations with P(id, fw-sim) or
                                P(bw-sim, id) are used. For quotienting,
                                LnLHw/LnHw-simulations are used. For modal
                                pruning, Lw/Hw-simulation is iteratively used,
                                deleting one edge at a time.""")
    parser.add_argument("direction", choices=["fw", "bw"],
                        help="direction of simulation used for reduction")
    parser.add_argument("-n", default=0, type=int,
                        help="""specify the 'n' in simulation
                                (default value is 0)""")
    args = parser.parse_args()

    hoaf = ""
    for line in sys.stdin:
        hoaf += line
    automaton = spot.automaton(hoaf)
    reduced = reduced_automaton(automaton, args)
    reduced.purge_dead_states()
    reduced.prop_reset()
    print(reduced.to_str("hoa", "1.1"))


def reduced_automaton(automaton, args):
    if args.technique == "prune":
        return pruning(automaton, args)
    elif args.technique == "quotient":
        return quotienting(automaton, args)
    elif args.technique == "modal_prune":
        return modal_pruning(automaton, args)
    return None


if __name__ == '__main__':
    main()
